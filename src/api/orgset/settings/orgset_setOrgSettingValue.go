/*
	Copyright: "NullTeam", 2016 - 2019
	Author: Nikita Ivanov <de1ay@nullteam.info>
*/
package settings

import (
	"net/http"
	"strconv"
	"strings"

	"nullteam.info/wplay/demo/conf"
	"nullteam.info/wplay/demo/src"
	"nullteam.info/wplay/demo/src/api/orgset"
)

func SetOrgSettingValue(token string, setting_name string, setting_value string, responseWriter http.ResponseWriter) bool {
	if orgset.IsUserAdmin(token, responseWriter) {
		organizationName, _, apiErr := orgset.GetUserOrganizationAndIdByToken(token)
		if apiErr != nil {
			return apiErr.Print(responseWriter)
		}
		src.CustomConnection = src.Connect_Custom(organizationName)
		response := setOrgSettingValue(setting_name, setting_value)
		response.Print(responseWriter)
	}
	return true
}

func setOrgSettingValue(setting_name string, setting_value string) *conf.ApiResponse {
	if len(setting_name) < 1 {
		return conf.ErrOrgSettingNameIncorrect
	}
	if len(setting_value) < 1 {
		return conf.ErrOrgSettingValueIncorrect
	}
	switch setting_name {
	case "participant":
		return setStringOrgSettingValue(setting_name, setting_value)
	case "team":
		return setStringOrgSettingValue(setting_name, setting_value)
	case "organization":
		return setStringOrgSettingValue(setting_name, setting_value)
	case "period":
		return setStringOrgSettingValue(setting_name, setting_value)
	case "self_marks":
		return setBooleanOrgSettingValue(setting_name, setting_value)
	case "patronymics":
		return setBooleanOrgSettingValue(setting_name, setting_value)
	case "emotional_mark_period":
		return setOrgSettingValue_EmotionalMarkPeriod(setting_value)
	default:
		return conf.ErrOrgSettingNameIncorrect
	}
}

func setStringOrgSettingValue(setting_name string, setting_value string) *conf.ApiResponse {
	query, err := src.CustomConnection.Prepare("UPDATE settings SET value=? WHERE name=?")
	if err != nil {
		return conf.ErrDatabaseQueryFailed
	}
	defer query.Close()
	_, err = query.Exec(setting_value, setting_name)
	if err != nil {
		return conf.ErrDatabaseQueryFailed
	}
	return conf.RequestSuccess
}

func setBooleanOrgSettingValue(setting_name string, setting_value string) *conf.ApiResponse {
	setting_value = strings.ToLower(setting_value)
	if setting_value != "true" && setting_value != "false" {
		return conf.ErrOrgSettingValueIncorrect
	}
	query, err := src.CustomConnection.Prepare("UPDATE settings SET value=? WHERE name=?")
	if err != nil {
		return conf.ErrDatabaseQueryFailed
	}
	defer query.Close()
	_, err = query.Exec(setting_value, setting_name)
	if err != nil {
		return conf.ErrDatabaseQueryFailed
	}
	return conf.RequestSuccess
}

func setOrgSettingValue_EmotionalMarkPeriod(setting_value string) *conf.ApiResponse {
	emotionalMarkPeriod, err := strconv.ParseInt(setting_value, 10, 64)
	if err != nil {
		return conf.ErrOrgSettingValueIncorrect
	}
	if emotionalMarkPeriod < 1 || emotionalMarkPeriod > 24 {
		return conf.ErrOrgSettingValueIncorrect
	}
	query, err := src.CustomConnection.Prepare("UPDATE settings SET value=? WHERE name='emotional_mark_period'")
	if err != nil {
		return conf.ErrDatabaseQueryFailed
	}
	defer query.Close()
	_, err = query.Exec(setting_value)
	if err != nil {
		return conf.ErrDatabaseQueryFailed
	}
	return conf.RequestSuccess
}
